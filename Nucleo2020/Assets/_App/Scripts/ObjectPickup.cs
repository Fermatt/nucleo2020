﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

using Valve.VR;

namespace VRPicking
{
    public class ObjectPickup : MonoBehaviour
    {

        [SerializeField]
        [FormerlySerializedAs("SphereOnOff")]
        private SteamVR_Action_Boolean sphereOnOff;

        [SerializeField]
        private SteamVR_Input_Sources handType;

        [SerializeField]
        private string objectTag;

        [SerializeField]
        private float throwStrength = 10;

        private bool isGrabbing = false;
        private Rigidbody grabbedObject = null;
        private Vector3 currentGrabbedLocation;

        public SteamVR_Action_Boolean SphereOnOff
        {
            get => sphereOnOff;
            set => sphereOnOff = value;
        }
        
        public SteamVR_Input_Sources HandType
        {
            get => handType;
            set => handType = value;
        }
        
        public string ObjectTag
        {
            get => objectTag;
            set => objectTag = value;
        }
        
        public float ThrowStrength
        {
            get => throwStrength;
            set => throwStrength = value;
        }
        
        private bool IsGrabbing
        {
            get => isGrabbing;
            set => isGrabbing = value;
        }
        
        private Rigidbody GrabbedObject
        {
            get => grabbedObject;
            set => grabbedObject = value;
        }
        
        private Vector3 CurrentGrabbedLocation
        {
            get => currentGrabbedLocation;
            set => currentGrabbedLocation = value;
        }

        private void Start()
        {
            SphereOnOff.AddOnStateDownListener(TriggerDown, HandType);
            SphereOnOff.AddOnStateUpListener(TriggerUp, HandType);
        }

        private void Update()
        {
            if (GrabbedObject != null)
            {
                CurrentGrabbedLocation = GrabbedObject.transform.position;

                if (IsGrabbing && 
                    GrabbedObject.transform.parent != transform)
                {
                    IsGrabbing = false;
                    GrabbedObject = null;
                }
            }
        }
        
        private void OnTriggerEnter(Collider other)
        {
            var mainObj = other.gameObject.GetComponentInParent<Rigidbody>();

            if (!IsGrabbing &&
                mainObj != null &&
                mainObj.CompareTag(ObjectTag))
            {
                GrabbedObject = mainObj;
            }
        }
        
        private void OnTriggerExit(Collider other)
        {
            var mainObj = other.gameObject.GetComponentInParent<Rigidbody>();

            if (!IsGrabbing &&
                mainObj != null &&
                mainObj == GrabbedObject)
            {
                GrabbedObject = null;
            }
        }
        
        public void TriggerDown(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
        {
            if (!IsGrabbing &&
                GrabbedObject != null)
            {
                // Set grab flag
                IsGrabbing = true;

                /*
                Rigidbody[] children = grabbedObject.GetComponentsInChildren<Rigidbody>();
                if (children.Length > 0)
                {
                    foreach (Rigidbody item in children)
                    {
                        item.transform.parent = null;
                    }
                }*/

                // Set parent and kinematic
                GrabbedObject.transform.SetParent(transform, true);
                GrabbedObject.isKinematic = true;

                // Cache grab location
                CurrentGrabbedLocation = GrabbedObject.transform.position;
            }
        }
        
        public void TriggerUp(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
        {
            if (IsGrabbing)
            {
                // Clear grab flag
                IsGrabbing = false;

                // Set parent and not kinematic
                GrabbedObject.transform.SetParent(null, true);
                GrabbedObject.isKinematic = false;

                // Set throw vector
                var throwVector = GrabbedObject.transform.position - CurrentGrabbedLocation;
                GrabbedObject.AddForce(throwVector * ThrowStrength, ForceMode.Impulse);
            }
        }
    }
}