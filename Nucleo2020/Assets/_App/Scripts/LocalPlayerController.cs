﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class LocalPlayerController : NetworkBehaviour
{
    [SerializeField] private GameObject rig;

    private Vector3 pos;

    // Start is called before the first frame update
    void Start()
    {
        if(!isLocalPlayer)
        {
            Destroy(rig);
            enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
