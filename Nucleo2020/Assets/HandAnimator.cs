﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class HandAnimator : MonoBehaviour
{
    private Animator anim;

    // a reference to the action
    public SteamVR_Action_Boolean CloseHand;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        CloseHand.AddOnStateDownListener(LeftTriggerDown, SteamVR_Input_Sources.LeftHand);
        CloseHand.AddOnStateDownListener(RightTriggerDown, SteamVR_Input_Sources.RightHand);

        CloseHand.AddOnStateUpListener(LeftTriggerUp, SteamVR_Input_Sources.LeftHand);
        CloseHand.AddOnStateUpListener(RightTriggerUp, SteamVR_Input_Sources.RightHand);
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void LeftTriggerDown(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
    {
        anim.SetInteger("AnimationState", 2);
    }

    private void RightTriggerDown(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
    {
        anim.SetInteger("AnimationState2", 2);

    }

    private void LeftTriggerUp(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
    {
        anim.SetInteger("AnimationState", 0);

    }

    private void RightTriggerUp(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
    {
        anim.SetInteger("AnimationState2", 0);

    }
}
