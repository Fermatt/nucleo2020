﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCameraMovement : MonoBehaviour
{
    [SerializeField] private Transform camera;
    [SerializeField] private float minMagnitude;

    // Update is called once per frame
    void Update()
    {
        if((transform.position - camera.position).magnitude > minMagnitude)
            transform.position = new Vector3(camera.position.x, transform.position.y, camera.position.z);
    }
}
