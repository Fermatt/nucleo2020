﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CalculateHeight : MonoBehaviour
{
    //[SerializeField] private float defaultHeight = 1.7f;
    [SerializeField] private Transform camera;
    private float currentHeight = 1;

    // Update is called once per frame
    void Update()
    {
        if (camera.position.y > currentHeight && camera.position.y < 2)
        {
            currentHeight = camera.position.y;
            transform.localScale = new Vector3(currentHeight, currentHeight, currentHeight);
        }
    }
}
